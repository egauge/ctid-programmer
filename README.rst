***************
CTid-programmer
***************

A graphical-user interface for programming CTid®-enabled sensors.

The avrdude enhancements which enable programming of ATtiny
microcontrollers with a standard FTDI serial cable have been merged
into the official release.  If you have avrdude v7 or newer installed,
you should be all set.
