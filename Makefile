%.py: %.ui
	pyside6-uic --from-imports $^ -o $@

%_rc.py: %.qrc
	pyside6-rcc -g python $^ -o $*_rc.py

TARGETDIR = /usr/lib/egauge/hw/CTid/src/target

ctid_programmer/resources/code/%.hex: $(TARGETDIR)/%.hex
	cp $^ $@

UIFILES = $(wildcard ctid_programmer/gui/*.ui)
UIPYFILES = $(UIFILES:.ui=.py)

QRCFILES = $(wildcard ctid_programmer/gui/*.qrc)
QRCPYFILES = $(patsubst %.qrc,%_rc.py,$(QRCFILES))

SRCS	= $(wildcard ctid_programmer/*.py)

HEXFILES = ac.hex powered.hex
CODETARGETS = $(addprefix ctid_programmer/resources/code/,$(HEXFILES))

all: $(UIPYFILES) $(QRCPYFILES) $(CODETARGETS)
	python3 setup.py sdist

clean:
	rm -f $(QRCPYFILES) $(UIPYFILES)

release: all
	twine upload dist/*

lint:
	pylint $(SRCS)
